<?php
/**
 * Super Plain Footer.
 *
 * @package WordPress
 * @subpackage Super_Plain
 * @since Super Plain 1.0
 */
?>
	</div> <!-- end container -->
	<div class="row">
		<div class="small-2 large-4 columns">
			<ul class="vcard">
				<li>Super Plain</li>
				<li><a href="#">superplain.com</a></li>
			</ul>
		</div>
		<div class="small-4 large-4 columns">
			<ul class="vcard">
				<li>Super Plain</li>
				<li><a href="#">superplain.com</a></li>
			</ul>
		</div>
		<div class="small-6 large-4 columns">
			<ul class="vcard">
				<li>Super Plain</li>
				<li><a href="#">superplain.com</a></li>
			</ul>
		</div>
		<div class="medium-12 columns">
			<p>copyright &copy; SuperPlain 2013</p>
		</div>
	</div>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/link.js"></script>
</body>
</html>
