<?php
/**
 * Super Plain Page.
 *
 * @package WordPress
 * @subpackage Super_Plain
 * @since Super Plain 1.0
 */
?>

<?php get_header(); ?>
		<div class="large-8 columns">
			<div class="panel">
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
				<div class="page-wrapper">
					<h2 class="entry-title"><?php the_title(); ?></h2>
					<?php the_content(); ?>
				</div>
			</div>
		</div>
<?php endwhile; ?>
	<?php get_sidebar(); ?>
<?php get_footer(); ?>
