<?php
/**
 * Super Plain Header.
 *
 * @package WordPress
 * @subpackage Super_Plain
 * @since Super Plain 1.0
 */
?>
<!DOCTYPE html>
<html lang="ja">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title><?php bloginfo('name'); ?></title>
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/normalize.css" media="screen" charset="utf-8" />
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/style.css" media="screen" charset="utf-8" />
<?php if (is_single()): ?>
	<link href="<?php bloginfo('template_url'); ?>/css/single.css" rel="stylesheet" type="text/css" />
<?php endif; ?>
	<?php wp_head(); ?>
</head>
<body>
	<div class="row">
		<div class="large-12 columns">
			<!-- Title -->
			<h1><a href="<?php bloginfo('url');?>">SuperPlain<small>logo</small></a></h1>
			<p class="subheader"><?php bloginfo('description');?></p>
			<!-- Title end -->
			<!-- Navigation -->
			<div>
				<?php wp_nav_menu(); ?>
			</div>
			<!-- Navigation End -->
		</div>
	</div>
	<div class="row">
