<?php
/**
 * Super Plain index.
 *
 * @package WordPress
 * @subpackage Super_Plain
 * @since Super Plain 1.0
 */
?>

<?php get_header(); ?>
		<div class="large-8 columns">
<?php while (have_posts()): the_post(); ?>
			<div class="panel">
				<!-- post start -->
				<div id="post-<?php the_ID(); ?>" class="post">
					<a href="<?php the_permalink(); ?>" class="blocklink">
						<div class="entry-content">
							<h3 class="entry-title-index"><?php the_title(); ?></h3>
							<?php the_excerpt(); ?>
						</div>
					</a>
					<!-- metaData start -->
					<div class="meta-data">
						<p class="entry-date"><?php the_time(__('Y年m月d日(D)')) ?></p>
						<p><?php comments_popup_link(__('Comments (0)'), __('Comments (1)'), __('Comments (%)')); ?></p>
					</div>
					<!-- metaData end -->
				</div>
				<!-- post end -->
			</div>
<?php endwhile; ?>
		</div>
		<?php get_sidebar(); ?>
<?php get_footer(); ?>
