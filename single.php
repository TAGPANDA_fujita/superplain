﻿<?php
/**
 * Super Plain Single.
 *
 * @package WordPress
 * @subpackage Super_Plain
 * @since Super Plain 1.0
 */
?>

<?php get_header(); ?>
		<div class="large-8 columns">
			<div class="panel">
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<h2 class="entry-title"><?php the_title() ?><small> by <?php the_author();?></small> </h2>
				<div <?php post_class(); ?> id="post-<?php the_ID(); ?>">
					<div class="entry-content">
						<?php the_content(); ?>
					</div> <!-- end entry-content -->
					<div class="comment-data">
						<hr />
						<p class="sub"><?php the_time(__('y年m月d日')) ?></p>
						<p><?php the_tags(); ?></p>
					</div> <!-- end comment-data -->
				</div> <!-- end post -->
			</div> <!-- end col2 entry -->
		</div>
	<!-- END ENTRY -->
<?php endwhile; endif; ?>
		<?php get_sidebar(); ?>
<?php get_footer(); ?>
