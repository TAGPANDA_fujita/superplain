<?php
/**
 * Super Plain Sidebar.
 *
 * @package WordPress
 * @subpackage Super_Plain
 * @since Super Plain 1.0
 */
?>
<div class="large-4 columns">
	<h4>最近の記事</h4>
	<!-- Recent Posts -->
	<?php query_posts('showposts=5'); ?>
	<ul class="side-nav">
<?php while (have_posts()) : the_post(); ?> 
	<li>
		<a href="<?php the_permalink() ?>"><?php the_title(); ?></a>
		<p><?php the_time('Y.m.d'); ?></p>
	</li>
<?php endwhile;?>
	</ul>
	<!-- END NAVIGATION -->	
</div> <!-- end sidebar -->
