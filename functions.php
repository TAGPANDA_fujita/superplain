<?php
/**
 * Super Plain functions and definitions.
 *
 * @package WordPress
 * @subpackage Super_Plain
 * @since Super Plain 1.0
 */

if ( function_exists( 'register_nav_menus' ) ) {
	register_nav_menus(
		array(
		  'pluginbuddy_mobile' => 'PluginBuddy Mobile Navigation Menu',
		  'foot_menu' => 'My Custom Footer Menu'
		)
	);
}
