$(function() {
	$('body').css('opacity', 0).animate({
		opacity: 1
	}, 800);

	$(document).on('click', 'a', function(e) {
		var moveUrl = $(this).attr('href');
		e.preventDefault();
		$('body').animate({
			opacity: 0
		}, 800);
		location.href = moveUrl;
	});
});
